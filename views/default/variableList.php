<div>
    <table class='table'>
        <thead>
        <tr>
            <td><b>Variable description</b></td>
            <td><b>Variable name</b></td>
        </tr>
        </thead>
        <tbody>
            <?php foreach($variableDescription as $variableName => $variableDesc): ?>
                <tr>
                    <td><?= $variableDesc ?></td>
                    <td>{{ <?= $variableName ?> }}</td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>