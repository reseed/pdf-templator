<?php
use yii\helpers\Html;

$this->title = Yii::t('pdfTemplator', 'Update {modelClass}: ', [
    'modelClass' => 'Template',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('pdfTemplator', 'Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('pdfTemplator', 'Update');
?>

<div class="template-update">
    <?= $this->render('_form', ['model' => $model, 'title' => $this->title, 'variableList' => $variableList ]) ?>
</div>
