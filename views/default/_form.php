<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use reseed\pdfTemplator\models\Template;
use conquer\codemirror\CodemirrorWidget;
use conquer\codemirror\CodemirrorAsset;
use kartik\icons\Icon;
use kartik\tabs\TabsX;
use yii\widgets\ActiveForm;
use reseed\reWidgets\rebox\ReBox;

Icon::map($this);

$selector = [
    'id' => [
        'modal' => 'ptModal',
        'modalTrigger' => 'ptModalTrigger',
        'showBarcode' => 'template-show_barcode',
        'barcodeType' => 'template-barcode_type'
    ],
];

$this->beginBlock('modalFrame');
Modal::begin([
    'toggleButton' => false,
    'header' => '<h3>' . yii::t('pdfTemplator', 'Variable List') . '</h3>',
    'id' => $selector['id']['modal'],
]);
if (isset($variableList)) {
    echo $variableList;
}
Modal::end();
$this->endBlock();

$this->registerJs('
    $("#' . $selector['id']['modalTrigger'] . '").click(function(event){
        $("#' . $selector['id']['modal'] . '").modal("toggle");
    });
    
    $("#' . $selector['id']['showBarcode'] . '").change(function(event){
        if($(this).val() == 0){
            $("#' . $selector['id']['barcodeType'] . '").prop("disabled", "disabled");
        } else {
            $("#' . $selector['id']['barcodeType'] . '").prop("disabled", false);
        }
    });
    
    var tabId = $("[data-toggle=\'tab\']").first().attr("href");
    $("#code-contents").appendTo(tabId);
    
    $("[data-toggle=\'tab\']").click(function(){
        tabId = $(this).attr("href");
        var lastChar = tabId.substr(tabId.length - 1);
        
        if($(tabId).children().length == 0){
            $("#code"+lastChar).removeClass("code-absolute");
            $("#code"+lastChar).appendTo(tabId);
        }
    });
');

$this->registerCss('
    .CodeMirror {
        height: 490px!important;
    }

    .code-relative{
        position: relative;
        z-index: 100;
    }

    .code-absolute {
        position: absolute;
        bottom: -15px;
        left: 0;
        z-index: 10;
    }
');
?>

<?php
ReBox::begin([
    'header' => [
        'options' => [
            'title' => Yii::t('pdfTemplator', $this->title)
        ],
        'icon' => [
            'name' => 'info-circle',
        ],
    ],
]);
?>

<?php $form = ActiveForm::begin(); ?>

<h4 class="text-info"><?= Html::encode(Yii::t('pdfTemplator', 'Template Info')); ?></h4>
<ul>
    <li>
        <?= yii::t('pdfTemplator', 'Customize your template using HTML, CSS and <b>twig variables and snippets</b>
                inset information from an order by inserting Twig variables into your template.');
        ?>
    </li>
    <li>
<?= yii::t('pdfTemplator', 'If you want to render variable with html, then you must use it like ') . '<strong>{{$var|raw}}</strong>';
?>
    </li>
    <li>
                <?= yii::t('pdfTemplator', 'In footer and header you can use some useful varibles:') ?>
        <ul>
            <li>
                <?= '{PAGENO} - ' . yii::t('pdfTemplator', 'Page number') . ';' ?>
            </li>
            <li>
<?= '{nbpg} - ' . yii::t('pdfTemplator', 'Number of pages') . ';' ?>
            </li>
        </ul>
    </li>
    <?=
    yii::t('pdfTemplator', 'If footer/header section containing 2 characters "|" - footer/header will be split into three strings and set as content for the left|centre|right parts of the footer/header (e.g. Chapter 1|{PAGENO}|Book Title). ') .
    Html::a(yii::t('pdfTemplator', 'Read more...'), 'http://mpdf1.com/manual/index.php?tid=151');
    ?>
</ul>

<div class="row">
    <div class="col-sm-3 col-xs-12">
        <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

        <?=
        $form->field($model, 'entity')->widget('\kartik\widgets\Select2', [
            'data' => Template::getEntityLabels(),
            'options' => [
                'placeholder' => Yii::t('common', 'Select entity ...'),
            ],
            'pluginEvents' => [
                'change' => "function() {
                            $.ajax({
                                method: 'POST',
                                url: '" . Url::to(['get-variable-list']) . "',
                                data: {entity: $(this).val()}
                            })
                            .done(function(data) {
                                $('#" . $selector['id']['modalTrigger'] . "').removeClass('hidden');
                                $('#" . $selector['id']['modal'] . " .modal-body').html(data);
                            })
                            .fail(function(data) {
                                console.log('server error!');
                            });
                        }",
            ]
        ]);
        ?>

        <?=
        Html::a(
        yii::t('pdfTemplator', 'View the variable list') . '<i class="glyphicon glyphicon-log-in"></i>', '#', [
            'id' => $selector['id']['modalTrigger'],
            'class' => isset($variableList) ? '' : 'hidden'
        ]
        );
        ?>

        <?= $form->field($model, 'language')->dropDownList(Template::getLanguages()) ?>
        <?= $form->field($model, 'format')->dropDownList(Template::getFormatlabel()) ?>
<?= $form->field($model, 'orientation')->dropDownList(Template::getOrientationsLabel()) ?>
<?= $form->field($model, 'show_barcode')->dropDownList(Template::getShowBarcodeLabel()) ?>
        <?= $form->field($model, 'barcode_type')->dropDownList(Template::getBarcodeTypeLabel()) ?>
        <?= $form->field($model, 'status_id')->dropDownList(Template::getStatusLabel()) ?>
    </div>

    <div class="col-sm-9 col-xs-12 form-group">
        <?php
        $items = [
            [
                'label' => Yii::t('pdfTemplator', 'Template'),
                'content' => '',
                'active' => true
            ],
            [
                'label' => Yii::t('pdfTemplator', 'Header'),
                'content' => ''
            ],
            [
                'label' => Yii::t('pdfTemplator', 'Footer'),
                'content' => ''
            ],
            [
                'label' => Yii::t('pdfTemplator', 'Css'),
                'content' => ''
            ]
        ];
        ?>

        <?=
        TabsX::widget([
            'items' => $items,
            'position' => TabsX::POS_ABOVE,
            'bordered' => true,
            'encodeLabels' => false
        ])
        ?>

        <div id="code-contents" class="code-relative">
            <div id="code0" class="code-relative">
                <?=
                $form->field($model, 'template')->widget(
                CodemirrorWidget::className(), [
                    'assets' => [
                        CodemirrorAsset::MODE_HTMLMIXED,
                        CodemirrorAsset::MODE_XML,
                        CodemirrorAsset::MODE_CSS,
                        CodemirrorAsset::MODE_JAVASCRIPT,
                        CodemirrorAsset::MODE_VBSCRIPT,
                        CodemirrorAsset::MODE_HTTP,
                    ],
                    'settings' => [
                        'lineNumbers' => true,
                        'mode' => 'htmlmixed',
                    ],
                    'options' => ['rows' => 50]
                ]
                )->label(false)
                ?>
            </div>

            <div id="code1" class="code-absolute">
                <?=
                $form->field($model, 'header')->widget(
                CodemirrorWidget::className(), [
                    'assets' => [
                        CodemirrorAsset::MODE_HTMLMIXED,
                        CodemirrorAsset::MODE_XML,
                        CodemirrorAsset::MODE_CSS,
                        CodemirrorAsset::MODE_JAVASCRIPT,
                        CodemirrorAsset::MODE_VBSCRIPT,
                        CodemirrorAsset::MODE_HTTP,
                    ],
                    'settings' => [
                        'lineNumbers' => true,
                        'mode' => 'htmlmixed'
                    ],
                ]
                )->label(false)
                ?>
            </div>

            <div id="code2" class="code-absolute">
                <?=
                $form->field($model, 'footer')->widget(
                CodemirrorWidget::className(), [
                    'assets' => [
                        CodemirrorAsset::MODE_HTMLMIXED,
                        CodemirrorAsset::MODE_XML,
                        CodemirrorAsset::MODE_CSS,
                        CodemirrorAsset::MODE_JAVASCRIPT,
                        CodemirrorAsset::MODE_VBSCRIPT,
                        CodemirrorAsset::MODE_HTTP,
                    ],
                    'settings' => [
                        'lineNumbers' => true,
                        'mode' => 'htmlmixed',
                    ],
                ]
                )->label(false)
                ?>
            </div>

            <div id="code3" class="code-absolute">
                <?=
                $form->field($model, 'css')->widget(
                CodemirrorWidget::className(), [
                    'assets' => [
                        CodemirrorAsset::MODE_HTMLMIXED,
                        CodemirrorAsset::MODE_XML,
                        CodemirrorAsset::MODE_CSS,
                        CodemirrorAsset::MODE_JAVASCRIPT,
                        CodemirrorAsset::MODE_VBSCRIPT,
                        CodemirrorAsset::MODE_HTTP,
                    ],
                    'settings' => [
                        'lineNumbers' => true,
                        'mode' => 'htmlmixed',
                        'lineWiseCopyCut' => true
                    ],
                ]
                )->label(false)
                ?>
            </div>
        </div>

    </div>
</div>

<row>
    <div class="form-group">
<?=
Html::submitButton($model->isNewRecord ?
 Yii::t('common', 'Create') : Yii::t('common', 'Update'), [
    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
]);
?>
    </div>
</row>

<?php ActiveForm::end(); ?>

<?php ReBox::end(); ?>