<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Persona;
use reseed\pdfTemplator\models\Template;

$this->title = Yii::t('pdfTemplator', 'Templates');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="template-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('pdfTemplator', 'Create Template'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'entity',
                'filter' => Template::getEntityLabels(),
            ],
            'title',
            'created_at',
            [
                'attribute' => 'created_by',
                'value' => function ($data) {
                    return Persona::getFullName($data->created_by);
                },
            ],
            'updated_at',
            [
                'attribute' => 'updated_by',
                'value' => function ($data) {
                    return Persona::getFullName($data->updated_by);
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
        ],
    ]); ?>
</div>
