<?php
use yii\helpers\Html;

$this->title = Yii::t('pdfTemplator', 'Create Template');
$this->params['breadcrumbs'][] = ['label' => Yii::t('pdfTemplator', 'Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="template-create">
    <?= $this->render('_form', ['model' => $model, 'title' => $this->title ]) ?>
</div>