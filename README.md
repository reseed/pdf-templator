#Pdf-templator module.

This module provides a GUI for PDF document creation in Rebecca web app. 

##Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Add

```
{
  "type": "git",
  "url": "git@bitbucket.org:reseed/pdf-templator.git"
},
```

to the `repositories` section of your `composer.json` and

```
"reseed/pdf-templator": "*",
```

to the `require` section of your `composer.json` file.

##Configuration

- Add module to backend config file

```
'modules' => [
    'pdfTemplator' => [
        'class' => 'reseed\pdfTemplator\Module'
    ]
]
```

- If you want to allow access in _frontend_ (with token) you must add module to _frontend_ config file

```
'modules' => [
    'pdfTemplator' => [
        'class' => 'reseed\pdfTemplator\Module'
    ]
]
```

- Run migrations:

```
php yii migrate --migrationPath=@reseed/pdfTemplator/migrations
```

## How to use:

- Save your data (Contract data etc.) to the storage (e.g. S3). You have some tools for this. See example DefaultController::actionTestSaveData.
- Get pdf document. You must use DefaultController::actionGetDocument (see method description). If you want to get pdf-document from frontend, then you must:
+   add module to frontend config (see above);
+   generate token for data link. See example DefaultController::actionTestCreateToken.
+   use DefaultController::actionGetDocument with additional param token=\*this_is_token\*

###Note:

- Your url to get document will look something like this: http://rebecca/backend/index.php/pdfTemplator/default/get-document?id=18&dest=preview for backend and http://rebecca/index.php/pdfTemplator/default/get-document?id=18&dest=preview&token=i_AM_smart_token!
In these url `dest` is destination, also you can use 'download' or 'preview' as parameters;