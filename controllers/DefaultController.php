<?php

namespace reseed\pdfTemplator\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use kartik\mpdf\Pdf;
use modules\sellOrder\models\SellOrder;
use modules\sellOrder\models\SellOrderContract;
use modules\buyOrder\models\BuyOrder;
use modules\buyOrder\models\BuyOrderContract;
use modules\cashflow\models\Invoice as CashflowInvoice;
use modules\inventory\models\InventoryItem;
use reseed\pdfTemplator\components\entity\Contract;
use reseed\pdfTemplator\components\entity\Invoice;
use reseed\pdfTemplator\components\JsonProcessor;
use reseed\pdfTemplator\components\PdfProcessor;
use reseed\pdfTemplator\components\storage\S3Storage;
use reseed\pdfTemplator\models\Template;
use reseed\pdfTemplator\models\TemplateLink;
use reseed\pdfTemplator\models\TemplateLinkToken;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error', 'get-document'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'create',
                            'update',
                            'index',
                            'delete',
                            'get-variable-list',
                            'get-template-links-by-template-id',
                            /*
                             *   Test
                             */
                            'test-save-data',
                            'save-as-pdf',
                            'contract-preview',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-variable-list' => ['post'],
                    'delete' => ['post'],
                    'get-template-links-by-template-id' => ['post'],
                ],
            ],
            'documentAccess' => [
                'class' => \reseed\pdfTemplator\components\filters\DocumentAccess::className(),
                'only' => ['get-document'],
            ],
        ];
    }

    /**
     * Lists all Template models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new \reseed\pdfTemplator\models\TemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }

    /**
     * Creates a new Template model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Template();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                    'model' => $model,
                ]);
        }
    }

    /**
     * Updates an existing Template model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                    'model' => $model,
                    'variableList' => $this->getVariableList($model->entity)
                ]);
        }
    }

    /**
     * Deletes an existing Template model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Action for variable list rendering.
     *
     * @author Andreev <andreev1024@gmail.com>
     *
     * @version ver 1.0 added on 2015-05-07
     *
     * @return mixed
     */
    public function actionGetVariableList()
    {
        $entityKey = yii::$app->request->post('entity');
        if (!isset($entityKey)) {
            throw new \Exception('Required attribute was skiped');
        }

        return $this->getVariableList($entityKey);
    }

    /**
     * Render variable list.
     *
     * @author Andreev <andreev1024@gmail.com>
     *
     * @version ver 1.0 added on 2015-05-07
     *
     * @param string $entityKey 'contract', 'invoice' etc.
     *
     * @return mixed
     */
    public function getVariableList($entityKey)
    {
        $class = Template::entityMap($entityKey);
        if (!$class) {
            throw new \Exception('Wrong attribute');
        }

        return $this->renderPartial('variableList', [
            'variableDescription' => $class::getVariableDescription()
            ]);
    }

    /**
     * Action returns document as file if $dest == download or in pdf viewer in browser if $dest == preview.
     *
     * @author Andreev <andreev1024@gmail.com>
     * @author Dmitry Fedorov <klka1@live.ru>
     *
     * @version ver 1.0 added on 2015-05-15
     *
     * @param int    $id         TemplateLink.id needed to return templates returned before
     * @param string $dest       The PDF output destination (download, preview - in browser, file - to save file as PDF etc.)
     * @param string $token
     * @param int    $entityId   Related record's id needed to create new template
     * @param string $entityName Related record's type needed to perform actionSaveData method
     * @param int    $templateId Template.id needed to perform actionSaveData method
     *
     * @return mixed
     */
    public function actionGetDocument($dest, $id = null, $token = null, $entityId = null, $entityName = null, $templateId = null)
    {
        if ($entityId !== null && $entityName !== null && $templateId !== null) {
            $this->actionSaveData($entityId, $entityName, $templateId);
            $temporaryTemplateLink = TemplateLink::find()->where([
                'template_id' => $templateId,
            ])
            ->orderBy([
                'created_at' => SORT_DESC,
            ])
            ->one();
            $id = $temporaryTemplateLink->id;
        }
        if ($templateLink = TemplateLink::findOne($id)) {
            $processor = new JsonProcessor(new S3Storage());
            $data = $processor->getData($templateLink);

            if ($template = $templateLink->template) {
                $time = time();
                $options = [
                    'filename' => "{$template->entity}_{$time}.pdf",
                ];

                $template->renderDocument($data, $dest, $options);
            }
        }

        throw new NotFoundHttpException(yii::t('pdfTemplator', 'The requested page does not exist.'));
    }

/**
 *   TESTs.
 */
    //  http://rebecca/backend/index.php/pdfTemplator/default/test-save-data?contractId=1&templateId=9
    public function actionTestSaveData($contractId, $templateId)
    {
        //  вызов из виджета (не реализован)
        //  собираем данные
        //  сохраняем в S3 (+сохраняем ссылку)
        $sellOrderContract = \modules\sellOrder\models\SellOrderContract::findOne($contractId);
        $template = $this->findModel($templateId);
        $processor = new JsonProcessor(new S3Storage());

        $entity = new \reseed\pdfTemplator\components\entity\Contract();
        $data = $entity->getData($sellOrderContract, $template);
        $processor->saveData($template, $data);
    }


    /**
     * Generate PDF tempalte and save as PDF file to S3.
     *
     * @author Mark <song@reseed-s.com>
     *
     * @version ver 1.0 added on 2015-06-16
     *
     * @param int    $entityId   Related record's id needed to create new template
     * @param string $entityName Related record's type needed to perform actionSaveData method
     * @param int    $templateId Template.id needed to perform actionSaveData method
     *
     * @return int File id
     */
    public function actionSaveAsPdf($entityId, $entityName, $templateId)
    {
        $processor = new PdfProcessor(new S3Storage());
        switch ($entityName) {
            case 'sellOrderContract':
                $model = SellOrderContract::findOne($entityId);
                $entity = new Contract();
                break;
            case 'buyOrderContract':
                $model = BuyOrderContract::findOne($entityId);
                $entity = new Contract();
                break;
        }

        if ($model && $entity) {
            $template = $this->findModel($templateId);
            $data = $entity->getData($model, $template);

            if ($template && $data) {
                $time = time();
                $options = [
                    'filename' => "{$template->entity}_{$time}.pdf",
                ];

                $fileData = $template->renderDocument($data, 'string', $options);

                return $processor->saveData($template, $fileData, $entityId);
            }
        }
    }

    /**
     * Generate PDF for review before creating contract.
     *
     * @author Mark <song@reseed-s.com>
     *
     * @version ver 1.0 added on 2015-06-16
     *
     * @param int    $orderId     order ID
     * @param int    $orderType   1:sellOrder 2:buyOrder
     * @param int    $templateId  Template.id needed to perform actionSaveData method
     * @param string $description contract description
     * @param string $issueDate   contract issueDate
     * @param string $expireDate  contract expireDate
     * @param string $signedBy    contract signedBy
     */
    public function actionContractPreview($orderId, $orderType, $templateId, $description = null, $issueDate = null, $expireDate = null, $signedBy = null)
    {
        if ($orderId !== null || $templateId !== null) {
            if ($orderType == InventoryItem::TYPE_SELL_ORDER) {
                $model = new SellOrderContract();
                
                if (!SellOrder::find()->where(['id'=>$orderId])->exists()) {
                    throw new NotFoundHttpException(yii::t('pdfTemplator', 'The requested page does not exist.'));
                }
            } else {
                $model = new BuyOrderContract();

                if (!BuyOrder::find()->where(['id'=>$orderId])->exists()) {
                    throw new NotFoundHttpException(yii::t('pdfTemplator', 'The requested page does not exist.'));
                }
            }

            $model->order_id = $orderId;
            $model->description = $description;
            $model->issue_date = $issueDate;
            $model->signed_by = $signedBy;
            $model->expire_date = $expireDate;
            $entity = new Contract();

            $template = $this->findModel($templateId);
            $processor = new JsonProcessor(new S3Storage());
            $data = $entity->getData($orderType, $model, $template);

            $time = time();
            $options = [
                'filename' => "{$template->entity}_{$time}.pdf",
            ];
            $dest = Pdf::DEST_BROWSER;
            $template->renderDocument($data, $dest, $options);
        }

        throw new NotFoundHttpException(yii::t('pdfTemplator', 'The requested page does not exist.'));
    }

    /**
     * Helping action needed to create new templateLink record.
     *
     * @param int    $entityId   stores related record's id
     * @param string $entityName stores related record's type name
     * @param int    $templateId stores Template.id
     */
    public function actionSaveData($entityId, $entityName, $templateId)
    {
        switch ($entityName) {
            case 'sellOrderContract':
                $model = SellOrderContract::findOne($entityId);
                $entity = new Contract();
                break;
            case 'sellOrderInvoice':
                $model = CashflowInvoice::findOne($entityId);
                $entity = new Invoice();
                break;
        }
        if ($model && $entity) {
            $template = $this->findModel($templateId);
            $processor = new JsonProcessor(new S3Storage());
            $data = $entity->getData($model, $template);
            $processor->saveData($template, $data, $entityId);
        }
    }

    public function actionTestCreateToken($linkId)
    {
        return TemplateLinkToken::generateToken($linkId, date('YmdHis', strtotime('+3 day')));
    }

    /**
     * Action helps to get templateLink's records by template_id column     *.
     *
     * @return string
     */
    public function actionGetTemplateLinksByTemplateId()
    {
        $items = [];
        $post = Yii::$app->request->post();
        $templateId = (int) $post['templateId'];
        $entityId = (int) $post['entityId'];
        $templateLinks = TemplateLink::find()
            ->where([
                'entity_id' => $entityId,
                'template_id' => $templateId,
            ])
            ->orderBy([
                'created_at' => SORT_DESC,
            ])
            ->all();
        /* $items = [
          'new' => Yii::t('pdfTemplator','Create new')
          ]; */
        foreach ($templateLinks as $templateLink) {
            $items[] = ['key' => $templateLink->id, 'value' => Yii::t('pdfTemplator', 'Template link from').' '.$templateLink->created_at];
        }
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return Json::encode($items);
    }

    /**
     * Finds the Template model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param int $id
     *
     * @return Template the loaded model
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Template::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(yii::t('pdfTemplator', 'The requested page does not exist.'));
        }
    }
}
