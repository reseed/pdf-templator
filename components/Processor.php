<?php
namespace reseed\pdfTemplator\components;

use reseed\pdfTemplator\models\Template;

/**
 * Processor is Bridge-pattern class
 */

abstract class Processor
{
    /**
     * Storage object
     * @var object
     */
    public $storage;

    public function __construct(storage\StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Save data in storage
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   Template $template
     * @param   $data
     * @param   $entityId
     */
    abstract public function saveData(Template $template, $data, $entityId);

    /**
     * Get data from storage
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   object $model
     */
    abstract public function getData($model);
}
