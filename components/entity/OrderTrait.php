<?php
namespace reseed\pdfTemplator\components\entity;

use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use modules\mediaContent\models\File;
use modules\sellOrder\models\SellOrder;

trait OrderTrait
{
    public static function getOrderVariableDescriptions()
    {
        return [
            'order.status' => yii::t('pdfTemplator', 'Order status'),
            'order.date' => yii::t('pdfTemplator', 'Order create date'),
            'order.grandTotalExclTax' => yii::t('pdfTemplator', 'Grand total exclude tax'),
            'order.grandTotalInclTax' => yii::t('pdfTemplator', 'Grand total include tax'),
            'order.grandTotalTax' => yii::t('pdfTemplator', 'Grand total tax'),
            'order.grandTotalQuantity' => yii::t('pdfTemplator', 'Grand total quantity'),
            'order.serial_number' => yii::t('pdfTemplator', 'Order serial number'),
            'order.customer.name' => yii::t('pdfTemplator', 'Customer name'),
            'order.customer.address' => yii::t('pdfTemplator', 'Customer address'),
            'order.billing.name' => yii::t('pdfTemplator', 'Billing name'),
            'order.billing.address' => yii::t('pdfTemplator', 'Billing address'),
            'items.*itemId*.priceExclTax' => yii::t('pdfTemplator', 'Item price exclude tax'),
            'items.*itemId*.priceInclTax' => yii::t('pdfTemplator', 'Item price include tax'),
            'items.*itemId*.subTotalExclTax' => yii::t('pdfTemplator', 'Subtotal exclude tax'),
            'items.*itemId*.subTotalInclTax' => yii::t('pdfTemplator', 'Subtotal include tax'),
            'items.*itemId*.quantity' => yii::t('pdfTemplator', 'Item quantity'),
            'items.*itemId*.SKU' => yii::t('pdfTemplator', 'SKU'),
            'items.*itemId*.qtyShipped' => yii::t('pdfTemplator', 'Item shipped quantity'),
            'items.*itemId*.taxAmount' => yii::t('pdfTemplator', 'Item tax amount'),
            'items.*itemId*.mainImg' => yii::t('pdfTemplator', 'Main image url'),
            'items.*itemId*.productName' => yii::t('pdfTemplator', 'Item product name'),
            'items.*itemId*.productDesc' => yii::t('pdfTemplator', 'Item product description'),
            'items.productOptions.*optionId*.name' => yii::t('pdfTemplator', 'Product option name'),
            'items.productOptions.*optionId*.price' => yii::t('pdfTemplator', 'Product option price'),
            'items.productOptions.*optionId*.unit' => yii::t('pdfTemplator', 'Product option unit'),
            'items.productOptions.*optionId*.variant' => yii::t('pdfTemplator', 'Product variant'),
        ];
    }

    /*
    Subtotals
        Discount                                -   skiped
        Discount Amount                         -   skiped

        *   Q:  Где брать Discount  и Discount amount ?
        *   A:  пока негде, но скоро будут

    Shipping
        Shipping (Excl. Tax)                    -   skiped
        Shipping (Incl. Tax)                    -   skiped
        Shipping Tax                            -   skiped

        *   у нас нет tax

    Customer
        Customer Name
        Email
        Customer Group
        First Name                              -   return null
        Last Name                               -   return null

        *   Q:  Customer. Это организация. Тогда зачем там last/first name?
        *   A:  nu mi je doljni obrashyatsya k konkretnomu cheloveku v etoi organizatsii
        *   Q:  как выбрать этого конкретного человека?
        *   A:  eto tot ot kogo mi poluchili zakaz, smotri customer_data
        *   Q:  у нас есть метод, которы на входе в sellOrder проверяет, принадлежит ли заказ кастомеру или организации, и если кастомеру - то переписывает его на организацию, а запись в customer_data удаляет
        *

    Template
        Logo
        Page Number (Header/Footer)
        Number of pages (Header/Footer)
        Print Date (Y-m-d - can be changed)

    Order
        Order Status
        Order Date

        Total Paid
        Total refunded                             -   skiped
        Total Due

        *   из Cashlow, но total refunded еще не готов


    Address
        Billing Address
        Shipping Address

    Shipping and Billing
        Billing Method                              -   skiped
        Shippment Tracking Code                     -   skiped

        *   пока не существует

    Item Variables
        Price Excl. Tax
        Price Incl. Tax
        Subtotal Excl. Tax
        Subtotal Incl. Tax
        Product Name
        SKU                                         -   Items.SKU == product.sku
        Quantity (Qty)
        Qty Ordered(order only)                     -   skiped Quantity (Qty) === Qty Ordered(order only)
        Qty Invoiced(order only)                    -   ???
        Qty Shipped(order only)
        Qty Refunded(order only)                    -   skiped
        Qty Canceled(order only)                    -   skiped
        Tax Amount
        Discount Amount                             -   skiped
        Product image
        Product options
        Product description
        Product url path                            -   skiped
    */

    /**
     * Collects and returns all necessary data by order
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   SellOrder $order
     * @return  array
     */
    public static function getOrderData(SellOrder $order)
    {
        $data['order'] = [
            'grandTotalExclTax' => 0,
            'grandTotalInclTax' => 0,
            'grandTotalTax' => 0,
            'items' => [],
        ];

        $allStatuses = \common\components\ConstantsComponent::getStatusOptions();
        $data['order']['status'] = isset($allStatuses[$order->status_id]) ? $allStatuses[$order->status_id] : null;
        $data['order']['date'] = $order->created_at;
        $data['order']['serial_number'] = $order->serial_number;
        //customer data
        $customer = $order->customer;
        $customer_addr = $customer->getAddresses()->one();
        $data['order']['customer'] = [
            'name' => $customer->org_name,
            'address' => implode(' ',
                array_filter(
                    $customer_addr->getAttributes(
                        [
                            'postal_code',
                            'area_level_1',
                            'area_level_2',
                            'locality',
                            'sublocality',
                            'street_number',
                            'premise',
                            'address_other',
                            'route',
                            'floor',
                            'room',
                        ]
                    )
                )
            )
        ];
        //organization data
        $organizaion = $order->sellOrderBinds->org;
        $organizaion_addr = $organizaion->getAddresses()->one();
        $data['order']['billing'] = [
            'name' => $organizaion->org_name,
            'address' => implode(' ',
                array_filter(
                    $organizaion_addr->getAttributes(
                        [
                            'postal_code',
                            'area_level_1',
                            'area_level_2',
                            'locality',
                            'sublocality',
                            'street_number',
                            'premise',
                            'address_other',
                            'route',
                            'floor',
                            'room',
                        ]
                    )
                )
            )
        ];

        $orderItems = $order->items;
        $shippingItems = [];
        $shippingRecords = $order->getShippingRecord()->asArray()->all();

        if ($shippingRecords) {
            $shippingItems = \modules\shipping\models\ShippingItem::find()
                ->where(['shipping_record_id' => ArrayHelper::getColumn($shippingRecords, 'id')])
                ->select(['quantity' => 'SUM(shipping_quantity)', 'sell_order_item_id'])
                ->groupBy('sell_order_item_id')
                ->asArray()
                ->indexBy('sell_order_item_id')
                ->all();
        }

        foreach ($orderItems as $oneItem) {
            $itemData = [];
            $optionData = [];
            $subTotal = $oneItem->getSubTotal();
            $tax = $oneItem->getTax();

            /**
            *   Grand Total
            */
            $data['order']['grandTotalExclTax'] += $subTotal;
            $data['order']['grandTotalInclTax'] += $oneItem->getTotal();
            $data['order']['grandTotalTax'] += $tax;
            
            /**
            *   Items
            */
            $itemData['priceExclTax'] = $oneItem->price;
            $itemData['priceInclTax'] = $oneItem->getTax($oneItem->price);
            $itemData['subTotalExclTax'] = $subTotal;
            $itemData['subTotalInclTax'] = $subTotal + $tax;
            $itemData['productName'] = $oneItem->product->title;
            $itemData['quantity'] = $oneItem->quantity;
            $itemData['SKU'] = $oneItem->product->code;
            $itemData['qtyShipped'] = isset($shippingItems[$oneItem->id]) ? $shippingItems[$oneItem->id]['quantity'] : 0;
            $itemData['taxAmount'] = $tax;
            $itemData['productDesc'] = $oneItem->product->description;
            $itemData['mainImg'] = ($mainImg = $oneItem->product->getMainImage()) ? $mainImg->getUrl() : null;

            /**
            *   Product Options
            */
            $itemOptions = $oneItem->getInventoryItemOption()->all();
            foreach ($itemOptions as $oneOption) {
                $optionData['name'] = $oneOption->option->name;
                $optionData['price'] = $oneOption->option_price;
                $optionData['unit'] = ($oneOption->option->unit !== 'none' ? ' ' . $oneOption->option->unit : '');
                
                if (!empty($oneOption->variant)) {
                    $optionData['variant'] = '* ' . $oneOption->variant->variant . $optionData['unit'];
                } elseif (!empty($oneOption->value)) {
                    $optionData['variant'] = '* ' . $oneOption->value . $optionData['unit'];
                } elseif (!empty($oneOption->option->value)) {
                    $optionData['variant'] = '* ' . $oneOption->option->value . $optionData['unit'];
                } else {
                    $optionData['variant'] = '';
                }
                
                $itemData['productOptions'][$oneOption->option_id] = $optionData;
            }

            $itemData['qtyInvoiced'] = null;
            $itemData['qtyRefunded'] = null;
            $itemData['qtyCanceled'] = null;
            $itemData['Discount Amount'] = null;
            $itemData['Product url path'] = null;
            
            $data['items'][$oneItem->id] = $itemData;
        }

        return $data;
    }
}
