<?php
namespace reseed\pdfTemplator\components\entity;

use yii;
use reseed\pdfTemplator\models\Template;

class Invoice extends AbstractEntity
{
    use OrderTrait;

    /**
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @return  string Entity name
     */
    public static function getName()
    {
        return yii::t('pdfTemplator', 'Invoice');
    }

    /**
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @return  string Variable description
     */
    public static function getVariableDescription()
    {
        $variableDescriptions = [
            'barcode' => yii::t('pdfTemplator', 'Barcode'),
        ];
        $variableDescriptions = array_merge($variableDescriptions, static::getOrderVariableDescriptions());
        return $variableDescriptions;
    }

    /**
     * Collects and returns all necessary data for current entity
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   object $entity for e.g SellOrderContract obj.
     * @param   Template $template
     * @return  array
     */
    public function getData($entity, Template $template)
    {
        $order = $entity->sellOrder;
        $this->changeLanguage($template->language);

        //  OrderTrait
        $data = static::getOrderData($order);
        $data['barcode'] = $entity->serial_number ? $entity->serial_number : null;

        $this->restoreLanguage();
        return $data;
    }
}
