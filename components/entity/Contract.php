<?php

namespace reseed\pdfTemplator\components\entity;

use yii;
use yii\helpers\Inflector;
use reseed\pdfTemplator\models\Template;
use modules\inventory\models\InventoryItem;

class Contract extends AbstractEntity
{
    use OrderTrait;

    protected static $attributes = [
        'description' => 'contract.description',
        'issue_date' => 'contract.issue_date',
        'signed_by' => 'contract.signed_by',
        'expire_date' => 'contract.expire_date',
    ];

    /**
     * @author Andreev <andreev1024@gmail.com>
     *
     * @version ver 1.0 added on 2015-05-07
     *
     * @return string Entity name
     */
    public static function getName()
    {
        return yii::t('pdfTemplator', 'Contract');
    }

    /**
     * @author Andreev <andreev1024@gmail.com>
     *
     * @version ver 1.0 added on 2015-05-07
     *
     * @return string Variable description
     */
    public static function getVariableDescription()
    {
        $attributes = static::$attributes;
        $attributes['barcode'] = 'barcode';
        foreach ($attributes as $attr => $placeholder) {
            $variableDescriptions[$placeholder] = yii::t('pdfTemplator', Inflector::camel2words($attr));
        }

        $variableDescriptions = array_merge($variableDescriptions, static::getOrderVariableDescriptions());

        return $variableDescriptions;
    }

    /**
    * Collects and returns all necessary data for current entity.
    *
    * @author Andreev <andreev1024@gmail.com>
    *
    * @version ver 1.0 added on 2015-05-07
    *
    * @param object   $entity   for e.g SellOrderContract obj.
    * @param Template $template
    *
    * @return array
    */
    public function getData($orderType, $entity, Template $template)
    {
        if ($orderType == InventoryItem::TYPE_SELL_ORDER) {
            $order = $entity->sellOrder;
            //  OrderTrait
            $data = static::getOrderData($order);
        } else {
            $order = $entity->buyOrder;
            $data = [];
        }

        $data['barcode'] = $entity->serial_number ? $entity->serial_number : null;
        foreach (static::$attributes as $attr => $placeholder) {
            if (isset($entity->$attr)) {
                $data['contract'][$attr] = $entity->$attr;
            }
        }

        $this->changeLanguage($template->language);
        $this->restoreLanguage();

        return $data;
    }
}
