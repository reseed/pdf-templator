<?php
namespace reseed\pdfTemplator\components\entity;

use yii;
use reseed\pdfTemplator\models\Template;

/**
 *  Abstrcat class for entities: Contract, Invoice etc.
 */
abstract class AbstractEntity
{
    /*
    protected $oldLanguage;
    */

    /**
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @return  string Entity name
     */
    public static function getName() {

    }

    /**
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @return  string Variable description
     */
    public static function getVariableDescription() {
        
    }

    /**
     * Collects and returns all necessary data for current entity
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   object $entity for e.g SellOrderContract obj.
     * @param   Template $template
     * @return  array
     */
    abstract public function getData($orderType, $entity, Template $template);

    /**
     * Change app language
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   string $locale
     * @return  null
     */
    protected function changeLanguage($locale)
    {
        $this->oldLanguage = Yii::$app->language;
        Yii::$app->language = $locale;
    }

    /**
     * Restore original app locale
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @return  boolean
     */
    protected function restoreLanguage()
    {
        if ($this->oldLanguage) {
            Yii::$app->language = $this->oldLanguage;
            return true;
        }

        return false;
    }
}