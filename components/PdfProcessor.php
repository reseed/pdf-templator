<?php
namespace reseed\pdfTemplator\components;

use yii;
use reseed\pdfTemplator\models\Template;
use yii\helpers\Json;
use kartik\mpdf\Pdf;

class PdfProcessor extends Processor
{

    /**
     * Save data in storage
     * @author Andreev <andreev1024@gmail.com>
     * @author Mark <song@reseed-s.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   Template $template
     * @param   array $data
     * @return  mixed
     */
    public function saveData(Template $template, $data, $entityId)
    {
        return $this->storage->save($template, $data, 'pdf', $entityId);
    }

    /**
     * Get data from storage
     * @author Andreev <andreev1024@gmail.com>
     * @author Mark <song@reseed-s.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   object $model
     * @return  mixed
     */
    public function getData($model)
    {
        $data = $this->storage->get($model);
        return $data ? $data: false;
    }
}
