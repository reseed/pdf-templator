<?php
namespace reseed\pdfTemplator\components\storage;

use yii;
use reseed\pdfTemplator\models\TemplateLink;
use reseed\pdfTemplator\models\Template;
use reseed\mediaContentManager\models\File;

class S3Storage implements StorageInterface
{
    /**
     * Return path for S3 storage
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @return  string
     */
    public function getPath()
    {
        return "contents/pdf";
    }

    /**
     * Return filename for S3 storage
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   Template $template
     * @param   string $ext File extension
     * @return  string
     */
    public function getFileName(Template $template, $ext)
    {
        $hash = Yii::$app->getSecurity()->generateRandomString();
        return "{$template->entity}_{$hash}.{$ext}";
    }

    /**
     * Save data in storage
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   Template $template [description]
     * @param   string $data
     * @param   string $ext File extension
     * @param   integer $entityId
     * @return  integer File ID
     */
    public function save(Template $template, $data, $ext, $entityId)
    {
        $s3path = $this->getPath($template);
        $filePath = Yii::getAlias('@runtime') . '/' . $this->getFileName($template, $ext);
        if(file_put_contents($filePath, $data)) {
            $fileModel = new File();
            $fileModel->setAttributes([
                'path' => $s3path,
                'file' => $filePath,
            ]);

            if (!$fileModel->save()) {
                unlink($filePath);
                return false;
            }

            unlink($filePath);
            $model = new TemplateLink;
            $model->url = $fileModel->getUrl();
            $model->entity_id = $entityId;
            $model->template_id = $template->id;
            $model->save();
            
            return $fileModel->id;
        }
        return false;
    }

    /**
     * Get data from S3 storage
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   TemplateLink $model
     * @throws \Exception
     * @return  string
     */
    public function get($model)
    {
        if ($model instanceof TemplateLink) {
            return file_get_contents($model->url);
        }

        throw new \Exception('Argument class is wrong');
    }
}
