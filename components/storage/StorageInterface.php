<?php
namespace reseed\pdfTemplator\components\storage;

use reseed\pdfTemplator\models\Template;

interface StorageInterface
{
    /**
     *
     * @param Template $template stores object of class reseed\pdfTemplator\models\Template
     * @param type $data stores data array for rendering
     * @param type $ext stores extension of file ( e.g. json )
     * @param type $entityId stores entityId of related record
     */
    public function save(Template $template, $data, $ext,$entityId);

    public function get($model);
}