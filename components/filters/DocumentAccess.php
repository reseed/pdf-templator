<?php
namespace reseed\pdfTemplator\components\filters;

use Yii;
use yii\base\ActionFilter;
use reseed\pdfTemplator\models\TemplateLinkToken;
use yii\web\ForbiddenHttpException;

class DocumentAccess extends ActionFilter
{
    /**
     * Check access for action
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   object $action
     * @return  boolean
     */
    public function beforeAction($action)
    {
        $id = yii::$app->request->get('id');
        $token = yii::$app->request->get('token');

        $accessFromFrontend = (yii::$app->id === 'app-frontend' and TemplateLinkToken::validateToken($id, $token));
        $accessFromBackend = yii::$app->id === 'app-backend';
        if ($accessFromFrontend || $accessFromBackend) {
            return true;
        } else {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }
}
