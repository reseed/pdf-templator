<?php
namespace reseed\pdfTemplator;

/**
 * @author Andreev <andreev1024@gmail.com>
 * @since 2015-05-07
 */

class Module extends \yii\base\Module
{
    /**
     * translate category for i18n
     * @var string
     */
    public $translateCategory = 'pdfTemplator';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
