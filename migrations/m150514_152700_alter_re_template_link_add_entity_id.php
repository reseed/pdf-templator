<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m150514_152700_alter_re_template_link_add_entity_id extends Migration
{
    public $tableName = '{{%template_link}}';

    protected $tableSchema;

    public function init()
    {
        parent::init();
        $this->tableSchema = $this->db->schema->getTableSchema($this->tableName, true);
    }

    public function safeUp()
    {
       if ($this->tableSchema !== null) {
            if (!in_array('entity_id', $this->tableSchema->columnNames))  {
                $this->addColumn($this->tableName, 'entity_id', Schema::TYPE_INTEGER);
            }
        }
    }

    public function safeDown()
    {
        if ($this->tableSchema !== null) {
            if (in_array('entity_id', $this->tableSchema->columnNames))  {
                $this->dropColumn($this->tableName, 'entity_id');
            }
        }
    }
}
