<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m150507_131659_create_re_template_link_token extends Migration
{
    public $tableName = '{{%template_link_token}}';
    protected $tableSchema;


    public function init()
    {
        parent::init();
        $this->tableSchema = $this->db->schema->getTableSchema($this->tableName, true);
    }

    public function safeUp()
    {
        if ($this->tableSchema === null) {
            $this->createTable($this->tableName, [
                'token' => Schema::TYPE_STRING."(255) NOT NULL",
                'link_id' => Schema::TYPE_INTEGER."(11) NULL",
                'expire_date' => Schema::TYPE_TIMESTAMP." NULL",
                'created_at' => Schema::TYPE_TIMESTAMP." NULL",
                'created_by' => Schema::TYPE_INTEGER."(11) NULL",
                'PRIMARY KEY (token)',
            ], $this->tableOptions);

            $this->addForeignKey('fk_re_template_link_token_link_id', $this->tableName, 'link_id', '{{%template_link}}', 'id');
        }
    }

    public function safeDown()
    {
        if ($this->tableSchema) {
            $this->dropForeignKey('fk_re_template_link_token_link_id', $this->tableName);
            $this->dropTable($this->tableName);
        }
        return true;
    }
}
