<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m150507_131125_create_re_template_link extends Migration
{
    public $tableName = '{{%template_link}}';
    protected $tableSchema;


    public function init()
    {
        parent::init();
        $this->tableSchema = $this->db->schema->getTableSchema($this->tableName, true);
    }

    public function safeUp()
    {
        if ($this->tableSchema === null) {
            $this->createTable($this->tableName, [
                'id' => Schema::TYPE_PK,
                'url' => Schema::TYPE_STRING."(255) NULL",
                'template_id' => Schema::TYPE_INTEGER."(11) NULL",
                'created_at' => Schema::TYPE_TIMESTAMP." NULL",
                'created_by' => Schema::TYPE_INTEGER."(11) NULL",
                'updated_at' => Schema::TYPE_TIMESTAMP." NULL",
                'updated_by' => Schema::TYPE_INTEGER."(11) NULL",
            ], $this->tableOptions);
            $this->addForeignKey('fk_re_template_link_template_id', $this->tableName, 'template_id', '{{%template}}', 'id');
        }
    }

    public function safeDown()
    {
        if ($this->tableSchema) {
            $this->dropForeignKey('fk_re_template_link_template_id', $this->tableName);
            $this->dropTable($this->tableName);
        }
        return true;
    }
}
