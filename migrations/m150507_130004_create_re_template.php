<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m150507_130004_create_re_template extends Migration
{
    public $tableName = '{{%template}}';

    protected $tableSchema;

    public function init()
    {
        parent::init();
        $this->tableSchema = $this->db->schema->getTableSchema($this->tableName, true);
    }

    public function safeUp()
    {
        if ($this->tableSchema === null) {
            $this->createTable($this->tableName, [
                'id' => Schema::TYPE_PK,
                'entity' => Schema::TYPE_STRING . "(255) NULL",
                'title' => Schema::TYPE_STRING . "(255) NULL",
                'template' => Schema::TYPE_TEXT . " NULL",
                'created_at' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT CURRENT_TIMESTAMP",
                'updated_at' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '0000-00-00 00:00:00'",
                'created_by' => Schema::TYPE_INTEGER . "(11) NOT NULL",
                'updated_by' => Schema::TYPE_INTEGER . "(11) NOT NULL",
                'status_id' => Schema::TYPE_SMALLINT . "(4) NULL",
                'header' => Schema::TYPE_TEXT . " NULL",
                'footer' => Schema::TYPE_TEXT . " NULL",
                'format' => Schema::TYPE_STRING . "(255) NULL",
                'orientation' => Schema::TYPE_STRING . "(255) NULL",
                'language' => Schema::TYPE_STRING . "(45) NULL",
                'show_barcode' => Schema::TYPE_SMALLINT . "(4) NULL",
                'barcode_type' => Schema::TYPE_STRING . "(255) NULL",
                'css' => Schema::TYPE_TEXT . " NULL",
            ], $this->tableOptions);
        }
    }

    public function safeDown()
    {
        if ($this->tableSchema) {
            $this->dropTable('{{%template}}');
        }
        return true;
    }
}
