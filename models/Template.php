<?php
namespace reseed\pdfTemplator\models;

use Yii;
use yii\db\Expression;
use kartik\mpdf\Pdf;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

class Template extends \yii\db\ActiveRecord
{
    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%template}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => new yii\db\Expression('NOW()'),
            ],
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity', 'title', 'template', 'status_id', 'format', 'orientation', 'language'], 'required'],
            [['template', 'header', 'footer', 'css'], 'string'],
            ['orientation', 'in', 'range' => array_keys(static::getOrientationsLabel())],
            ['format', 'in', 'range' => array_keys(static::getFormatLabel())],
            ['language', 'in', 'range' => array_keys(static::getLanguages())],
            ['status_id', 'in', 'range' => array_keys(static::getStatusLabel())],
            ['show_barcode', 'in', 'range' => array_keys(static::getShowBarcodeLabel())],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'safe'],
            [['created_by', 'updated_by', 'status_id'], 'integer'],
            [['entity', 'title'], 'string', 'max' => 255],
            ['barcode_type', 'validateBarcodeType'],
        ];
    }

    public static function getOrientationsLabel()
    {
        return [
            Pdf::ORIENT_PORTRAIT => yii::t('pdfTemplator', 'Portrait'),
            Pdf::ORIENT_LANDSCAPE => yii::t('pdfTemplator', 'Landscape'),
        ];
    }

    public static function getFormatLabel()
    {
        return [
            Pdf::FORMAT_A3 => 'A3',
            Pdf::FORMAT_A4 => 'A4',
            Pdf::FORMAT_LETTER => yii::t('pdfTemplator', 'Letter'),
            Pdf::FORMAT_LEGAL => yii::t('pdfTemplator', 'Legal'),
            Pdf::FORMAT_FOLIO => yii::t('pdfTemplator', 'Folio'),
            Pdf::FORMAT_LEDGER => yii::t('pdfTemplator', 'Ledger-L'),
            Pdf::FORMAT_TABLOID => yii::t('pdfTemplator', 'Tabloid'),
        ];
    }

    public static function getBarcodeTypeLabel()
    {
        return [
            'ISBN' => 'EAN-13',
            'UPCA' => 'UPC-A',
            'UPCE' => 'UPC-E',
            'EAN8' => 'EAN-8',
            'IMB' => 'Intelligent Mail barcode',
            'RM4SCC' => 'Royal Mail 4-state Customer barcode',
            'KIX' => 'Royal Mail 4-state Customer barcode (Dutch)',
            'POSTNET' => 'POSTNET',
            'PLANET' => 'PLANET',
            'C128A' => 'Code 128',
            'EAN128A' => 'UCC/EAN-128 (GS1-128)',
            'C39' => 'Code 39 (Code 3 of 9)',
            'S25' => 'Standard 2 of 5',
            'I25' => 'Interleaved 2 of 5',
            'C93' => 'Code 93',
            'MSI' => 'MSI',
            'CODABAR' => 'CODABAR',
            'CODE11' => 'Code 11',
        ];
    }

    public static function getStatusLabel()
    {
        return [
            static::STATUS_ENABLE => yii::t('pdfTemplator', 'Enable'),
            static::STATUS_DISABLE => yii::t('pdfTemplator', 'Disable'),
        ];
    }

    public static function getShowBarcodeLabel()
    {
        return [
            static::STATUS_ENABLE => yii::t('pdfTemplator', 'Yes'),
            static::STATUS_DISABLE => yii::t('pdfTemplator', 'No'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('pdfTemplator', 'ID'),
            'entity' => Yii::t('pdfTemplator', 'Entity'),
            'title' => Yii::t('pdfTemplator', 'Title'),
            'template' => Yii::t('pdfTemplator', 'Template'),
            'header' => Yii::t('pdfTemplator', 'Header'),
            'footer' => Yii::t('pdfTemplator', 'Footer'),
            'orientation' => Yii::t('pdfTemplator', 'Orientation'),
            'format' => Yii::t('pdfTemplator', 'Format'),
            'language' => Yii::t('pdfTemplator', 'Language'),
            'status_id' => Yii::t('pdfTemplator', 'Status'),
            'css' => Yii::t('pdfTemplator', 'Css'),
            'created_at' => Yii::t('pdfTemplator', 'Created At'),
            'updated_at' => Yii::t('pdfTemplator', 'Updated At'),
            'created_by' => Yii::t('pdfTemplator', 'Created By'),
            'updated_by' => Yii::t('pdfTemplator', 'Updated By'),
            'show_barcode' => Yii::t('pdfTemplator', 'Show barcode'),
            'barcode_type' => Yii::t('pdfTemplator', 'Barcode type'),
        ];
    }

    /**
     * Entity map. If you want to add new entity (e.g. Invoice),
     * then you must add it in this method.
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   mixed $entity
     * @return  mixed
     */
    public static function entityMap($entity = null)
    {
        $map = [
            'contract' => '\reseed\pdfTemplator\components\entity\Contract',
            'invoice' => '\reseed\pdfTemplator\components\entity\Invoice'
        ];

        if (isset($entity)) {
            return $map[$entity];
        } else {
            return $map;
        };
    }

    /**
     * Return entity labels
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @return  array
     */
    public static function getEntityLabels()
    {
        $labels = [];
        foreach (array_keys(static::entityMap()) as $entityKey) {
            $labels[$entityKey] = yii::t('pdfTemplator', ucfirst($entityKey));
        }
        return $labels;
    }

    /**
     * Get App languages
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @return  array
     */
    public static function getLanguages()
    {
        $languages = \common\models\Language::find()
            ->where(['enable'=>1])
            ->all();

        return ArrayHelper::map($languages, 'code', 'title_en');
    }

    /**
     * Render Twig template
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  private
     * @param   array $data Data for template variable
     * @return  string
     */
    private function renderTemplate($data)
    {
        $twig = new \Twig_Environment(
            null,
            [
            //  'cache' => 'runtime/mpdf',
                'charset' => Yii::$app->charset,
            ]
        );
        $twig->addExtension(new \Twig_Extensions_Extension_Intl());
        $twig->setLoader(new \Twig_Loader_String());
        $this->renderBarcode($data);
        $this->selectiveSkipEscaping();

        $twig_template = $twig->loadTemplate($this->template);
        return $twig_template->render($data);
    }

    /**
     * Render document
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   array $data Data for template variable
     * @param   string $destination The PDF output destination (download, show in browser etc.)
     * @param   array $options
     * @return  mixed
     */
    public function renderDocument($data, $destination, $options)
    {
        $destination = strtoupper($destination);
        if (! $destination = $this->getDestination($destination)) {
            return false;
        }

        $content = $this->renderTemplate($data);
        $config = [
            // set to use core fonts only
            // 'mode' => Pdf::MODE_CORE,
            'format' => $this->format,
            'orientation' => $this->orientation,
            'destination' => $destination,
            'content' => $content,
            'cssInline' => $this->css,
            // set mPDF properties on the fly
            'options' => [
                'title' => $this->title,
                //  http://mpdf1.com/manual/index.php?tid=507
                'autoScriptToLang' => true,
                'autoLangToFont' => true,
            ],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader' => [$this->header],
                'SetFooter' => [$this->footer],
            ],
            'filename' => $options['filename'] ? $options['filename'] : $this->entity . '_' . time() . '.pdf',
        ];

        $pdf = new Pdf($config);
        return $pdf->render();
    }

    /**
     * Barcode rendering.
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  private
     * @param   array &$data Data for template variable
     * @return  boolean
     * @todo Skiped params: height, text, pr (http://mpdf1.com/manual/index.php?tid=407)
     */
    private function renderBarcode(&$data)
    {
        if ((int)$this->show_barcode !== static::STATUS_ENABLE or !isset($data['barcode'])) {
            $data['barcode'] = '';
            return false;
        }

        $data['barcode'] = '<barcode code="' . $data['barcode'] . '" type="' . $this->barcode_type . '" />';
        return true;
    }

    /**
     * Transform url param `destination` (e.g. 'download')
     * in mpdf destination (e.g 'D')
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   string $destination
     * @return  mixed
     */
    public function getDestination($destination)
    {
        $destination = strtolower($destination);
        $availableDestination = [
            'preview' => Pdf::DEST_BROWSER,
            'download' =>  Pdf::DEST_DOWNLOAD,
            'file' =>  Pdf::DEST_FILE,
            'string' =>  Pdf::DEST_STRING,
            'i' => Pdf::DEST_BROWSER,
            'd' =>  Pdf::DEST_DOWNLOAD,
            'f' =>  Pdf::DEST_FILE,
            's' =>  Pdf::DEST_STRING
        ];

        if (isset($availableDestination[$destination])) {
            return $availableDestination[$destination];
        }

        return false;
    }

    /**
     * Method returns list of available destination types
     * @author Dmitry Fedorov <klka1@live.ru>
     * @version ver 1.0 added on 2015-05-14
     * @access  public
     * @return  array
     */
    public static function getDestinationList() {
        return [
            'preview' => Yii::t('pdfTemplator', 'preview'),
            'download' => Yii::t('pdfTemplator', 'download')
        ];
    }

    /**
     * Barcode type validator
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   string $attribute
     * @param   array $params
     * @return  null
     */
    public function validateBarcodeType($attribute, $params)
    {
        $barcodeTypes = $this->getBarcodeTypeLabel();
        if (! isset($barcodeTypes[$this->$attribute])) {
            $this->addError($attribute, yii::t('pdfTemplator', 'Invalid barcode type.'));
        }
    }

    /**
     * Allows skip ecaping for some twig varibles
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  protected
     * @return  null
     */
    protected function selectiveSkipEscaping()
    {
        $tags = [
            'barcode',
        ];

        $tags = implode('|', $tags);
        $this->template = preg_replace("#{{\s*({$tags})\s*}}#i", '{{$1|raw}}', $this->template);
    }
}
