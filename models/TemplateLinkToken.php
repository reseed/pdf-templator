<?php

namespace reseed\pdfTemplator\models;

use Yii;

/**
 * This is the model class for table "{{%template_link_token}}".
 *
 * @property string $token
 * @property integer $link_id
 * @property string $expire_date
 * @property string $created_at
 *
 * @property TemplateLink $link
 */
class TemplateLinkToken extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%template_link_token}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['token', 'link_id', 'expire_date'], 'required'],
            [['link_id'], 'integer'],
            ['link_id',  'exist', 'targetAttribute' => 'id', 'targetClass' => TemplateLink::className()],
            [['created_at'], 'safe'],
            [['token'], 'string', 'max' => 255]
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'updatedAtAttribute' => false,
                'value' => new yii\db\Expression('NOW()'),
            ],
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => false,
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'token' => 'Token',
            'link_id' => 'Link ID',
            'expire_date' => 'Expire Date',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(TemplateLink::className(), ['id' => 'link_id']);
    }

    /**
     * Generate token.
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   string $id TemplateLink.id
     * @param   string $expire_date
     * @return  mixed
     */
    public static function generateToken($id, $expire_date)
    {
        $token = Yii::$app->getSecurity()->generateRandomString();
        $model = new static();
        $model->expire_date = $expire_date;
        $model->token = $token;
        $model->link_id = $id;

        if ($model->save()) {
            return $token;
        }

        return false;
    }

    /**
     * Token validator
     * @author Andreev <andreev1024@gmail.com>
     * @version ver 1.0 added on 2015-05-07
     * @access  public
     * @param   string $id TemplateLink.id
     * @param   string $token
     * @return  boolean
     */
    public static function validateToken($id, $token)
    {
        return (bool)static::find()
            ->where(['link_id'=>$id, 'token' => $token])
            ->andWhere(['>=', 'expire_date', date('YmdHis')])
            ->count();
    }
}
