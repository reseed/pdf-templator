<?php
namespace reseed\pdfTemplator\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use reseed\pdfTemplator\models\Template;

class TemplateSearch extends Template
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['entity', 'title', 'template', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Template::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            // 'created_at' => $this->created_at,
            // 'updated_at' => $this->updated_at,
            // 'created_by' => $this->created_by,
            // 'updated_by' => $this->updated_by,
        ]);
        if ($this->created_by) {
            $userQuery = User::find()
                ->select('id')
                ->where(['like', 'username', $this->created_by])
                ->groupBy('id');
            $query->andFilterWhere(['in', 'created_by', $userQuery]);
        }
        if ($this->updated_by) {
            $userQuery = User::find()
                ->select('id')
                ->where(['like', 'username', $this->updated_by])
                ->groupBy('id');
            $query->andFilterWhere(['in', 'updated_by', $userQuery]);
        }
        $query->andFilterWhere(['like', 'created_at', $this->created_at]);
        $query->andFilterWhere(['like', 'updated_at', $this->created_at]);

        $query->andFilterWhere(['like', 'entity', $this->entity])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'template', $this->template]);

        return $dataProvider;
    }
}
